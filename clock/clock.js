class Clock {
    static updateInterval = 0;

    constructor({ anchorElement, hourColor, minuteColor, secondColor, size, strokeWidth, inset }) {
        if (!anchorElement || !(anchorElement instanceof HTMLElement))
            throw new TypeError("anchorElement needs to be a html element");
        
        this.anchorElement = anchorElement;

        this.colors = {
            hour: hourColor !== null && typeof hourColor !== "undefined" ? hourColor : "white",
            minute: minuteColor !== null && typeof minuteColor !== "undefined" ? minuteColor : "white",
            second: secondColor !== null && typeof secondColor !== "undefined" ? secondColor : "white"
        };

        this.size = size === null || typeof size !== "number" ? 250 : size;
        this.strokeWidth = strokeWidth === null || typeof strokeWidth !== "number" ? 5 : strokeWidth;
        this.inset = inset === null || typeof inset !== "number" || inset < 2 ? 2 : inset;
        this.radius = this.size / 2 - this.strokeWidth - this.inset;
        this.circumference = 2 * this.radius * Math.PI;

        this._setup();
        this.start();
    }

    _createIntervalCircle(divides) {
        const radius = this.radius + this.strokeWidth / 2;
        const margin = this.inset + this.strokeWidth / 2;
        const offset = this.size / 2;

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.classList.add("dividends");

        for (var i = 0; i < divides; i++) {
            const angle = 360 * (i / divides);
            const radians = angle / 180 * Math.PI;

            const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
            path.setAttribute("d", "m " + (offset + Math.cos(radians) * radius) + " " + (offset + Math.sin(radians) * radius) + " l " + Math.cos(radians) * margin + " " + Math.sin(radians) * margin);

            svg.appendChild(path);
        }

        return svg;
    }

    _getDot() {
        let dot = document.createElement("div");

        dot.style.width = this.radius * 2;
        dot.style.height = this.radius * 2;
        dot.style.top = (this.size - this.radius * 2) / 2;
        dot.style.left = (this.size - this.radius * 2) / 2;
        dot.classList.add("dot");

        return dot;
    }

    _getCircle() {
        let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");

        circle.setAttribute("cx", this.size / 2);
        circle.setAttribute("cy", this.size / 2);
        circle.setAttribute("r", this.radius);

        circle.style.strokeWidth = this.strokeWidth;

        return circle;
    }

    _setupHourElement() {
        const hourAnchor = document.createElement("div");
        hourAnchor.classList.add("time-display");
        hourAnchor.setAttribute("time-prop", "hours");
        hourAnchor.style.setProperty("--color", this.colors.hour);
        hourAnchor.style.width = this.size;
        hourAnchor.style.height = this.size;

        this.hourDot = this._getDot();

        this.hourCirlce = this._getCircle();
        this.hourCirlce.style.strokeDasharray = this.circumference;
        this.hourCirlce.style.strokeDashoffset = this.circumference;

        this.hourElement = document.createElement("span");
        this.hourElement.innerHTML = "00";

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");

        svg.appendChild(this._getCircle());
        svg.appendChild(this.hourCirlce);
        
        hourAnchor.appendChild(this.hourDot);
        hourAnchor.appendChild(svg);
        hourAnchor.appendChild(this._createIntervalCircle(12));
        hourAnchor.appendChild(this.hourElement);

        return hourAnchor;
    }

    _setupMinuteElement() {
        const minuteAnchor = document.createElement("div");
        minuteAnchor.classList.add("time-display");
        minuteAnchor.setAttribute("time-prop", "minutes");
        minuteAnchor.style.setProperty("--color", this.colors.minute);
        minuteAnchor.style.width = this.size;
        minuteAnchor.style.height = this.size;

        this.minuteDot = this._getDot();

        this.minuteCirlce = this._getCircle();
        this.minuteCirlce.style.strokeDasharray = this.circumference;
        this.minuteCirlce.style.strokeDashoffset = this.circumference;

        this.minuteElement = document.createElement("span");
        this.minuteElement.innerHTML = "00";

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");

        svg.appendChild(this._getCircle());
        svg.appendChild(this.minuteCirlce);
        
        minuteAnchor.appendChild(this.minuteDot);
        minuteAnchor.appendChild(svg);
        minuteAnchor.appendChild(this._createIntervalCircle(12));
        minuteAnchor.appendChild(this.minuteElement);

        return minuteAnchor;
    }

    _setupSecondElement() {
        const secondAnchor = document.createElement("div");
        secondAnchor.classList.add("time-display");
        secondAnchor.setAttribute("time-prop", "seconds");
        secondAnchor.style.setProperty("--color", this.colors.second);
        secondAnchor.style.width = this.size;
        secondAnchor.style.height = this.size;

        this.secondDot = this._getDot();

        this.secondCirlce = this._getCircle();
        this.secondCirlce.style.strokeDasharray = this.circumference;
        this.secondCirlce.style.strokeDashoffset = this.circumference;

        this.secondElement = document.createElement("span");
        this.secondElement.innerHTML = "00";

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");

        svg.appendChild(this._getCircle());
        svg.appendChild(this.secondCirlce);
        
        secondAnchor.appendChild(this.secondDot);
        secondAnchor.appendChild(svg);
        secondAnchor.appendChild(this._createIntervalCircle(12));
        secondAnchor.appendChild(this.secondElement);

        return secondAnchor;
    }

    _setup() {
        this.anchorElement.appendChild(this._setupHourElement());
        this.anchorElement.appendChild(this._setupMinuteElement());
        this.anchorElement.appendChild(this._setupSecondElement());

        this.timeMode = document.createElement("span");
        this.anchorElement.appendChild(this.timeMode);
    }

    updateTime(hours, minutes, seconds) {
        let h = hours;
        let m = minutes;
        let s = seconds;

        let hh = h < 10 ? "0" + h : h;
        let mm = m < 10 ? "0" + m : m;
        let ss = s < 10 ? "0" + s : s;

        if (this.hourElement)
            this.hourElement.innerHTML = hh;
        
        if (this.minuteElement)
            this.minuteElement.innerHTML = mm;

        if (this.secondElement)
            this.secondElement.innerHTML = ss;
    }

    updateCircles(hours, minutes, seconds, milliseconds) {
        const msHours   = (milliseconds + 1000 * (seconds + 60 * (minutes + 60 * (hours))));
        const msMinutes = (milliseconds + 1000 * (seconds + 60 * (minutes)));
        const msSeconds = (milliseconds + 1000 * (seconds));

        const hourPart = 12 * 60 * 60 * 1000;
        const minutePart = hourPart / 12;
        const secondPart = minutePart / 60;

        this.hourCirlce.style.strokeDashoffset = this.circumference - (this.circumference * (msHours / hourPart));
        this.minuteCirlce.style.strokeDashoffset = this.circumference - (this.circumference * (msMinutes / minutePart));
        this.secondCirlce.style.strokeDashoffset = this.circumference - (this.circumference * (msSeconds / secondPart));

        this.hourDot.style.transform = "rotate(" + 360 * (msHours / hourPart) + "deg)";
        this.minuteDot.style.transform = "rotate(" + 360 * (msMinutes / minutePart) + "deg)";
        this.secondDot.style.transform = "rotate(" + 360 * (msSeconds / secondPart) + "deg)";
    }

    update() {
        const date = new Date();

        const _h = date.getHours();

        const h = _h > 12 ? _h - 12 : _h;
        const m = date.getMinutes();
        const s = date.getSeconds();
        const ms = date.getMilliseconds();

        if (this.timeMode)
            this.timeMode.innerHTML = _h > 12 ? "pm" : "am";

        this.updateTime(h, m, s);
        this.updateCircles(h, m, s, ms);
    }

    start() {
        this.update();
        this.intervalId = setInterval(() => this.update(), Clock.updateInterval);
    }

    stop() {
        clearInterval(this.intervalId);
    }
}