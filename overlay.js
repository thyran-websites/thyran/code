$.prototype.overlay = function(mode = "show", data) {
	if (!($(this).hasClass("overlay"))) return;
	
	switch(mode) {
	case "show":
		$(this).trigger("overlay:show", data);
		$(this).addClass("show");
		break;
	case "hide":
		$(this).trigger("overlay:hide", data);
		$(this).removeClass("show");
		break;
	}
};

$("*.overlay:not(.noclose)").on("click", function(e) {
	e.stopPropagation();
	
	if (!($(this).hasClass("noclose-content") && $(e.originalEvent.target).closest("div.overlay-content").length > 0))
		$(this).overlay("hide");
});

$("*.overlay").on("transitionend  webkitTransitionEnd", function(e) {
	if ($(this).hasClass("show")) $(this).trigger("overlay:shown");
	else $(this).trigger("overlay:hidden");
});

$(window).on("keydown", function(e) {
	if (e.key === "Escape")
		$(".overlay").overlay("hide");
});