$(function() {
	$("*.sidebar-container:not(.sidebar-left):not(.sidebar-right)").addClass("sidebar-left");
	
	$("nav.sidebar > ul.sidebar-nav > li.sidebar-item").each(function() {
		if ($(this).children().length == 0 && !($(this).hasClass("sidebar-title") || $(this).hasClass("sidebar-toggle")))
			$(this).remove();
		else if ($(this).find("span.sidebar-item-img").length == 0 && $(this).find("a.sidebar-text").length > 0) {
			let prep = $("<span class='sidebar-item-img sidebar-letter'>" + $($(this).find("a.sidebar-text")[0]).text().substring(0, 1) + "</span>")
			let wrapper = $("<div class='sidebar-item-img-container'></div>");
			
			wrapper.append(prep);
			
			$(this).prepend(wrapper);
		} else if (!$(this).hasClass("sidebar-toggle") && $(this).find("span.sidebar-item-img").length > 0 && $(this).find("div.sidebar-item-img-container").length == 0)
			$(this).find("span.sidebar-item-img").wrap("<div class='sidebar-item-img-container'></div>");
	});
	
	if ($("nav.sidebar > ul.sidebar-nav.sidebar-bottom").length == 0)
		$("nav.sidebar").append($("<ul class='sidebar-nav sidebar-bottom'><li class='sidebar-item sidebar-toggle'><span class='sidebar-item-img sidebar-toggle-img'></span><a class='sidebar-text'>Collapse</a></li></ul>"));
	
	var dbRequest = indexedDB.open("sidebar-settings");
	dbRequest.onupgradeneeded = function(event) {
		var db = dbRequest.result;
		db.createObjectStore("data", { keyPath: "name" });
	};
	dbRequest.onsuccess = function(event) {
		$("div.sidebar-container[sidebar-save]").each(function() {
			var ref = $(this);
			
			if ($(this).attr("sidebar-name")) {
				var transaction = dbRequest.result.transaction("data").objectStore("data").get($(this).attr("sidebar-name"));
				transaction.onsuccess = function(event) {
					var result = transaction.result;
					
					if (!result)
						$(ref.find("li.sidebar-item")[0]).trigger("sidebar:change");
					else {
						ref.find("li.sidebar-item[data-tab='" + result.tab + "']").trigger("sidebar:change");
						ref.children("nav.sidebar").toggleClass("active", result.active);
					}
				};
			} else {
				$("div.sidebar-container").each(function() {
					$(this).find("li.sidebar-item.active").trigger("sidebar:change");
				});
			}
		});
	};
	
	$("div.sidebar-container").find("li.sidebar-item:not(.sidebar-button *)").on("sidebar:change", function(e) {
		e.stopPropagation();
		
		$(this).closest("nav.sidebar > ul.sidebar-nav.sidebar-tabs").find("li.sidebar-item.active").removeClass("active");
		$("*.sidebar-container ul.sidebar-nav-tabs > li.active").removeClass("active");
		
		var activeItem = $(this).closest("nav.sidebar > ul.sidebar-nav.sidebar-tabs > li.sidebar-item");
		activeItem.addClass("active");
		$("#" + activeItem.attr("data-tab")).addClass("active");
		
		$("#" + activeItem.attr("data-tab")).trigger("sidebar:change");
	});

	$("div.sidebar-container").find("li:not(.sidebar-button *)").on("click", function(e) {
		e.stopPropagation();
		
		if ($(this).closest("nav.sidebar").length == 0 && !$("div.sidebar-container > nav.sidebar").hasClass("sidebar-static"))
			$("nav.sidebar").removeClass("active");
		else if ($(this).closest("nav.sidebar > ul.sidebar-nav > li.sidebar-item[href]:not(.sidebar-toggle)").length > 0)
			window.location.href = $(this).closest("nav.sidebar > ul.sidebar-nav > li.sidebar-item[href]:not(.sidebar-toggle)").attr("href");
		else if ($(this).closest("nav.sidebar > ul.sidebar-nav > li.sidebar-toggle").length > 0)
			$(this).closest("nav.sidebar").toggleClass("active");
		
		else if ($(this).closest("nav.sidebar > ul.sidebar-nav.sidebar-tabs > li.sidebar-item:not(.sidebar-button)").length > 0) {
			$(this).trigger("sidebar:change");
		}
	});
});

$(window).on("beforeunload", function(e) {
	var dbRequest = indexedDB.open("sidebar-settings");
	dbRequest.onupgradeneeded = function(event) {
		var db = dbRequest.result;
		db.createObjectStore("data", { keyPath: "name" });
	};
	dbRequest.onsuccess = function(event) {
		$("div.sidebar-container[sidebar-save]").each(function() {
			var ref = $(this);
			
			var transaction = dbRequest.result.transaction("data", "readwrite").objectStore("data").put({
				name: $(this).attr("sidebar-name"),
				tab : $(this).find("li.active").attr("data-tab"),
				active: $(this).children("nav.sidebar").hasClass("active")
			});
		});
	};
});