$(function() {
	$("li.dropdown-menu-item").each(function() {
		var dropdownList = $(this).children("div.dropdown-submenu").children("ul.dropdown-list");
		if (dropdownList.length == 0 || dropdownList.children("li.dropdown-menu-item").length == 0)
			$(this).children("div.dropdown-submenu").remove();
		
		$(this).children("a.dropdown-menu-text").wrapInner("<div class='dropdown-menu-text-container'></div>");
		
		if ($(this).children("div.dropdown-submenu").length > 0) {
			var dropdownIndicator = $("<span class='dropdown-submenu-indicator'></span>");
			$(this).children("a.dropdown-menu-text").append(dropdownIndicator);
		}
	});
});