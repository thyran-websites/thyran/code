if (jQuery) {
    jQuery.fn.extend({
        progressbar: function(color = "blue") {
            this.each(function() {
                $(this).attr("progress-color", color);
                $(this).addClass("progress-bar");
                $(this).append("<div class='progress'></div>");
            });
        },
        progress: function(progress = 0) {
            this.each(function() {
                if ($(this).is(".progress-bar")) {
                    $(this).find("div.progress").css({
                        width: progress + "%"
                    });
                }
            });
        }
    });
}