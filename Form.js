class Form {
    constructor(form) {
        this.form = form;
    }

    serialize() {
        let result = {};

        this.form.querySelectorAll("input, select, textarea").forEach(element => {
            let name = element.name;
            let value = element.value;

            if (name)
                result[name] = value;
        });

        return Object.keys(result).length == 0 ? null : result;
    }

    reset() {
        this.form.querySelectorAll("input, select, textarea").forEach(element => {
            element.value = "";
        });

        return true;
    }
	
	validate(json, validCallback) {
		this.form.dispatchEvent(new CustomEvent("valid", {
			detail: {
				value: json,
				valid: typeof validCallback == typeof Function ? validCallback(json) : validCallback
			}
		}));
	}
}