if (jQuery) {
    jQuery.fn.extend({
        slideAlert: function(alertType = null) {
            this.each(function() {
                $(this).addClass("alert");
                $(this).addClass("alert-slide");

                if (alertType)
                    $(this).attr("type", alertType);
            });

            return true;
        }
    });
}