$(function() {
	$("a.scroll-target").on("click", function(event) {
		if (this.hash !== "") {
			event.preventDefault();
			
			var duration = 300;
			var durationFactor = (2 / 3);
			var defaultOffset = 50;
			
			var hash = this.hash;
			var offset = ($(".scroll-highlight-offset").length > 0 ? $(".scroll-highlight-offset").height() : 0);
			var scrollBody = $(".scroll-highlight-body");
			
			if ($(hash).length == 0) return;
			
			scrollBody.animate({
				scrollTop: $(hash).offset().top + scrollBody.scrollTop() - offset - defaultOffset
			}, {
				duration: duration,
				start: function() {
					setTimeout(function() {
						var animateionObj = $($(hash).find(".scroll-highlight")[0]);
						animateionObj.addClass("active");
					}, duration * durationFactor);
				}
			});
		}
	});
	
	$("a.scroll-target").find("*").on("click", function(event) {
		event.preventDefault();
		
		$(this).closest("a.scroll-target").trigger("click");
	});
	
	$(".scroll-highlight").on("webkitAnimationEnd oanimationend msAnimationEnd animationend", function() {
		$(this).removeClass("active");
	});
});