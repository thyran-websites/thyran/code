function createInputContainer(element) {
    const input = element.querySelector("input.nice-input");
    const placeholder = element.querySelector("span.nice-input-placeholder");

    if (input) {
        /**
         * 
         * @param {Event} event 
         */
        function toggleInputOpen(event) {
            event.target.classList.toggle("open", event.target.value !== "");
        }
        
        input.addEventListener("input", toggleInputOpen);
        input.addEventListener("change", toggleInputOpen);

        input.classList.toggle("open", input.value !== "");
    }

    if (placeholder) {
        placeholder.addEventListener("click", function(event) {
            event.stopPropagation();
            console.log(event);
            event.target.parentElement.querySelector("input.nice-input").focus();
        });
    }
}

function updateInputContainers() {
    document.querySelectorAll(".nice-input-container").forEach(createInputContainer);
}

updateInputContainers();