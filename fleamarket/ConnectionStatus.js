window.addEventListener("load", () => {
    let className = "connection-status";

    let attributeName = "status";

    let possibleValues = ["offline", "error", "online"];
    let defaultValue = possibleValues[0];

    Array.from(document.getElementsByClassName(className)).forEach(element => {
        if (!element.getAttribute(attributeName))
            element.setAttribute(attributeName, defaultValue);
    });

    Array.from(document.getElementsByClassName(className)).forEach(element => {
        element.addEventListener(attributeName, event => {
            console.log(event.detail.status);

            if (possibleValues.includes(event.detail.status))
                element.setAttribute(attributeName, event.detail.status);
            else
                console.warn("Connection status cannot be set for " + element + ": value is not accepted!\n" + "Please use one of the following " + possibleValues);
        });
    });
});