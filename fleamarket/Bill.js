class Bill {
    constructor(bill) {
        this.bill = bill;
    }

    add(seller, price) {
        let newElement = document.createElement("tr");

        let sellerElement = document.createElement("td");
        let priceElement = document.createElement("td");

        sellerElement.innerHTML = seller;
        sellerElement.classList.add("seller");

        priceElement.innerHTML = Number.parseFloat(price).format();
        priceElement.classList.add("price");

        newElement.appendChild(sellerElement);
        newElement.appendChild(priceElement);

        this.bill.appendChild(newElement);

        if (this.total)
            this.total.dispatchEvent(new CustomEvent("update", {
                detail: {
                    method: "add",
                    price: price
                }
            }));

        return Array.from(this.bill.getElementsByTagName("tr")).length - 1;
    }

    setTotalElement(total) {
        this.total = total;

        this.total.classList.add("price");
        this.total.addEventListener("update", e => {
            let method = e.detail.method;

            if (["add", "remove"].includes(method)) {
                let price = Number.parseFloat(e.detail.price);

                let prevPrice = Number.parseFloat(this.total.innerHTML.replace(",", "\."))

                console.log(method);
                console.log(price);

                let methodFactor = method == "add" ? 1 : -1;

                this.total.innerHTML = (prevPrice + price * methodFactor).format();
            } else
                console.warn("Bill cannot update total amount due to lack of a correct method.\nUse 'add' or 'remove'");
        });
    }

    remove(id) {
        let list = Array.from(this.bill.getElementsByTagName("tr"));

        if (id >= list.length || id < 0) return false;

        let element = list[id];
        let price = element.getElementsByClassName("price")[0].innerHTML;

        this.bill.removeChild(element);
        
        if (this.total)
            this.total.dispatchEvent(new CustomEvent("update", {
                detail: {
                    method: "remove",
                    price: price
                }
            }));
        
        return true;
    }

    removeLast() {
        return this.remove(Array.from(this.bill.getElementsByTagName("tr")).length - 1);
    }

    clear() {
        while(this.removeLast());
    }
}