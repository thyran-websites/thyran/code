const LogType = {
	SUCCESS: "success",
	MESSAGE: "msg",
	WARNING: "warning",
	ERROR  : "error"
};

function Log(selector, args) {
	var row = $("<tr>");
	row.append("<th topic='time'>time</th>");
	row.append("<th topic='msg'>message</th>");
	row.append("<th topic='type'>type</th>");
	row.append("<th topic='details'>details</th>");
	
	var header = $("<thead>");
	header.append(row);
	
	this._object = $("<table class='log scroll'>");
	this._object.append(header);
	this._object.append("<tbody>");
	
	var details = $("<div class='log-details'>");
	var itemCount = $("<div class='log-item-count'>");
	var pageSize = $("<div class='log-page-size'>");
	
	this._pageCount = $("<div class='log-page-count'>");
	this._count = $("<span>");
	this._pageSize = $("<input type='number'>");
	
	itemCount.append(this._count);
	pageSize.append(this._pageSize);
	
	details.append(this._pageCount);
	details.append(itemCount);
	details.append(pageSize);
	
	selector.append(this._object);
	selector.append(details);
	
	this._log = [];
	this._maxPageSize = args !== undefined && "maxPageSize" in args ? args.maxPageSize : 10;
	
	this._currentPage = 1;
	this._pageSize.val(this._maxPageSize);
};

Log.prototype.log = function(type, data, details) {
	var detailList = $("<ul class='details'>");
	
	if (details)
		Object.keys(details).forEach((key, index) => {
			detailList.append("<li class='detail'><div class='detail-key'>" + key + "</div><a class='detail-value'>" + details[key] + "</a></li>");
		});
	
	var detailContainer = $("<td topic='details'>");
	detailContainer.append(detailList);
	
	var newItem = $("<tr data-type='" + type + "'>");
	newItem.append("<td topic='time'>" + new Date(Date.now()).toLocaleTimeString() + "</td>");
	newItem.append("<td topic='msg'>" + data + "</td>");
	newItem.append("<td topic='type'>" + type + "</td>");
	newItem.append(detailContainer);
	
	this._log.push({
		type: type,
		data: data,
		date: Date.now(),
		item: newItem,
		details: details,
		id: this._log.length
	});
	
	if (this._log.length > this._maxPageSize * this._currentPage)
		newItem.css("display", "none");
	
	this._object.children("tbody").append(newItem);
	
	this._counter.max.text(Math.ceil(this._log.length / this._maxPageSize));
	this._count.text(this._log.length);
};

Log.prototype.setMaxPageSize = function(size) {
	if (size < 1) return this;
	
	this._maxPageSize = size;
	
	this._object.find("tbody > tr[data-type]").css("display", "none");
	
	this._log.filter(item => {
		var lowerBound = item.id >= (this._currentPage - 1) * this._maxPageSize;
		var upperBound = item.id < this._currentPage * this._maxPageSize;
		
		return lowerBound && upperBound;
	}).forEach((item, index) => {
		item.item.css("display", "");
	});
	
	this._counter.max.text(Math.ceil(this._log.length / this._maxPageSize));
	
	if (size * (this._currentPage - 1) >= this._log.length)
		this.offsetPage(Math.ceil(this._log.length / this._maxPageSize) - this._currentPage);
	
	return this;
};

Log.prototype.changePage = function(pageID) {
	if (pageID < 1 || pageID > Math.ceil(this._log.length / this._maxPageSize)) return;
	
	this._object.find("tbody > tr[data-type]").css("display", "none");
	this._currentPage = pageID;
	
	this._counter.current.text(this._currentPage);
	
	this._log.filter(item => {
		var lowerBound = item.id >= (this._currentPage - 1) * this._maxPageSize;
		var upperBound = item.id < this._currentPage * this._maxPageSize;
		
		return lowerBound && upperBound;
	}).forEach((item, index) => {
		item.item.css("display", "");
	});
};

Log.prototype.offsetPage = function(steps)  { this.changePage(this._currentPage + steps); };

Log.prototype.addPageChange = function() {
	this._counter = {
		current: $("<a topic='current'>" + this._currentPage + "</a>"),
		max    : $("<a topic='max'>" + this._currentPage + "</a>")
	};
	
	var firstButton = $("<div class='center' topic='first'><span>&Larr;</span></div>");
	var prevButton  = $("<div class='center' topic='prev'><span>&larr;</span></div>");
	var nextButton  = $("<div class='center' topic='next'><span>&rarr;</span></div>");
	var lastButton  = $("<div class='center' topic='last'><span>&Rarr;</span></div>");
	
	var ref = this;
	
	firstButton.on("click", function() { ref.changePage(1); });
	prevButton .on("click", function() { ref.offsetPage(-1); });
	nextButton .on("click", function() { ref.offsetPage(1); });
	lastButton .on("click", function() { ref.changePage(Math.ceil(ref._log.length / ref._maxPageSize)); });
	
	var counter = $("<div class='center' topic='count'><div class='log-page-counter'></div></div>");
	counter.children("div.log-page-counter").append(this._counter.current);
	counter.children("div.log-page-counter").append("/");
	counter.children("div.log-page-counter").append(this._counter.max);
	
	this._pageCount.append(firstButton);
	this._pageCount.append(prevButton);
	this._pageCount.append(counter);
	this._pageCount.append(nextButton);
	this._pageCount.append(lastButton);
	
	this._pageSize.on("input change", function() { ref.setMaxPageSize($(this).val()); });
	
	return this;
};

Log.prototype.clear = function() {
	this._log.forEach((data, index) => {
		data.item.remove();
	});
	
	this._log.splice(0, this._log.length);
	
	return this;
};

$.prototype.log = function(args) {
	return new Log($(this), args).addPageChange();
};