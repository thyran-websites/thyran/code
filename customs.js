Number.prototype.round = function(places) {
	return +(Math.round(this + "e+" + places)  + "e-" + places);
};

Number.prototype.pad = function(size) {
	if (size === null)
		size = 2;
	
	var s = String(this);
    while (s.length < size)
		s = "0" + s;
	
    return s;
};

Number.prototype.format = function() {
	const separate = 3;
	
	var s = String(this.toFixed(2));
	var sign = "";
	
	if (s.substring(0, 1) == "-") {
		sign = "-";
		s = s.substring(1, s.length);
	}
	
	s = s.replace(".", ",");
	
	var tmp = s.split(",");
	var number  = tmp[0];
	var decimal = (tmp[1] == undefined ? "00" : tmp[1]);
	
	var digitCount = Math.floor(number.length / separate);
	var digitStart = number.length % separate;
	
	if (digitStart == 0) {
		digitCount--;
		digitStart = 3;
	}
	
	if (digitCount > 0)
		for(var i = 0; i < digitCount; i++)
			number = number.substring(0, digitStart + i * (separate + 1)) + "." + number.substring(digitStart + i * (separate + 1));
	
	return sign + number + "," + decimal;
};

Date.prototype.toDateString = function() {
	return this.getFullYear() + "-" + (this.getMonth() + 1).pad() + "-" + (this.getDate()).pad();
};

Date.prototype.subtractYear = function(years) {
	return new Date(this.getFullYear() - years, this.getMonth(), this.getDate(), this.getHours(), this.getMinutes(), this.getSeconds(), this.getMilliseconds());
};

Date.prototype.onlyDate = function() {
	return new Date(this.getFullYear(), this.getMonth(), this.getDate());
};

Date.prototype.getDayMonth = function() {
	return this.getDate().pad() + "." + (this.getMonth() + 1).pad();
};