function returnHTTPParams() {
    let url = window.location.href;

    if (url.includes("?")) {
        let params = url.substring(url.indexOf("?") + 1);

        let list = params.split("&");
        let result = {};

        list.forEach(e => {
            let pair = e.split("=");
            
            let key = pair[0];
            let value = pair[1];

            result[key] = value;
        });

        return result;
    }

    return null;
}