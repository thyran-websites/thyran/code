// credit: https://stackoverflow.com/a/9763697

$(function() {
	$(".option").each(function() {
		$(this).toggleClass("wrapped", $("#options").width() <= 300);
	});
});

// css transition callback
$(".option .option-content").on("transitionEnd webkitTransitionEnd transitionend oTransitionEnd msTransitionEnd", function(e) {
	if ($(this).hasClass("show")) {
		$(this).css("max-height", 99999); // try setting this to 'none'... I dare you!
	}
});

$(".option span.option-toggle, .option .option-header .option-name > span:not([disabled])").on("click", function(e) {
	var content = $(this).closest(".option").children(".option-content");
	content.inner = content.children(".option-body");
	
	content.toggleClass("show hide");
	content.contentHeight = content.outerHeight();
	
	content.closest(".option").find("span.option-toggle").text(content.hasClass("show") ? "Collapse" : "Expand");
	
	if (content.hasClass("hide")) {
        // disable transitions & set max-height to content height
		content.removeClass("transitions").css("max-height", content.contentHeight);
		setTimeout(function() {
			// enable & start transition
			content.addClass("transitions").css({
				"max-height": 0,
				"opacity": 0,
				"margin-bottom": 0
			});
		}, 10); // 10ms timeout is the secret ingredient for disabling/enabling transitions
		// chrome only needs 1ms but FF needs ~10ms or it chokes on the first animation for some reason
	} else if (content.hasClass("show")) {
		content.contentHeight += content.inner.outerHeight(); // if closed, add inner height to content height
		content.addClass("transitions").css({
			"max-height": content.contentHeight,
			"opacity": 1,
			"margin-bottom": 20
		});
	}
});

$(window).on("resize", function() {
	$(".option").each(function() {
		$(this).toggleClass("wrapped", $("#options").width() <= 300);
	});
});