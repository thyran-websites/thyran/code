if (jQuery) {
    (function(func) {
        $.fn.addClass = function() {
            func.apply(this, arguments);

            this.trigger("classAdded");
            return this;
        };
    })($.fn.addClass);

    (function(func) {
        $.fn.removeClass = function() {
            func.apply(this, arguments);

            this.trigger("classRemoved");
            return this;
        };
    })($.fn.removeClass);
}